""" Billow Barista v0.1 """

# Modules
import random
import os
from time import sleep

# Lists
barista_list = []
not_kool = ["Well nobody saw that coming", "Not kool bro", "slavery.", "This is not N. Korea"]

# Functions
def give_names():
    os.system("clear")
    print "Let's get some names for your potential barista's"
    print ""
    while True:
        print "(You can just hit enter with no input once you are done)"
        name = raw_input("Give me a name: ")
        if name:
            barista_list.append(name)
            b_list = ", ".join(barista_list)
            os.system("clear")
            print "Our list so far: %s" % b_list
            print ""
        else:
            return 0

def find_barista(name):
    if barista_list:
        os.system("clear")
        print "Your barista is..."
        print ""
        sleep (3)
        if len(barista_list) <= 1:
            print random.choice(not_kool)
        elif len(barista_list) > 1:
            print "~~** %s **~~" % random.choice(name)
            print ""
    else:
        return 0

# This is where we start
give_names()
find_barista(barista_list)
