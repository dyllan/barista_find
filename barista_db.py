import sqlite3
from prettytable import *

sqlite_file = 'barista.db'

def barista_stats_update(bname,bcount,ts):
    conn = sqlite3.connect(sqlite_file)
    c = conn.cursor()
    c.execute('CREATE TABLE IF NOT EXISTS barista(Barista TEXT PRIMARY KEY, Count INT, Latest TEXT)')
    c.execute('INSERT OR REPLACE INTO barista VALUES(?, ?, ?);', (bname, bcount, ts))
    conn.commit()
    conn.close()

def barista_stats_fetch():
    conn = sqlite3.connect(sqlite_file)
    c = conn.cursor()
    c.execute('CREATE TABLE IF NOT EXISTS barista(Barista TEXT PRIMARY KEY, Count INT, Latest TEXT)')
    c.execute('SELECT * from barista')
    barista_table = from_db_cursor(c)
    print barista_table.get_string(sortby="Count", reversesort=True)
    conn.close()

def barista_stats_check(name):
    conn = sqlite3.connect(sqlite_file)
    c = conn.cursor()
    c.execute('CREATE TABLE IF NOT EXISTS barista(Barista TEXT PRIMARY KEY, Count INT, Latest TEXT)')
    c.execute('SELECT * from barista WHERE Barista = ?;', (name,))
    rows = c.fetchall()
    for row in rows:
        stats = row[1]
        return stats
    conn.close()
