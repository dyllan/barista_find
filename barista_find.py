""" Barista v0.2 """

import random, os, time, math, datetime
from termcolor import colored
from barista_db import *

option_list = ["1","2","3","4","5","6","7"]
barista_list = []
weight_name = ""
weight = 0
not_kool = ["Well nobody saw that coming", "Not kool bro"]

def logo_print():
    print colored(" ___ _ _ _              ___           _    _        ", "red")
    print colored("| _ |_) | |_____ __ __ | _ ) __ _ _ _(_)__| |_ __ _ ", "yellow")
    print colored("| _ \ | | / _ \ V  V / | _ \/ _` | '_| (_-<  _/ _` |", "green")
    print colored("|___/_|_|_\___/\_/\_/  |___/\__,_|_| |_/__/\__\__,_| v0.2", "blue")
    print "\n"

def barista_menu():
    while True:
        os.system("clear")
        logo_print()
        print "Menu"
        print "___________________________________"
        print colored("[1] Add a barista to the list", "green")
        print "[2] Remove a barista from the list"
        print "[3] View the barista list"
        print colored("[4] Randomly select a barista", "blue")
        print "[5] View barista stats"
        print "[6] Weight your baristas " + colored("(buggy)", "red")
        print colored("[7] Exit", "red")
        print "___________________________________"
        print "\n"
        option = raw_input("Select menu option: ")
        if any(item == option for item in option_list):
            if option == "1":
                name = raw_input("Enter a name to add: ")
                barista_add(name)
            elif option == "2":
                if len(barista_list) < 1:
                    print colored("Nothing to remove, list empty", "red")
                    time.sleep(2)
                    barista_menu()
                else:
                    name = raw_input("Enter a name to delete: ")
                    barista_del(name)
            elif option == "3":
                barista_view()
            elif option == "4":
                barista_find(barista_list)
            elif option == "5":
                barista_stats()
            elif option == "6":
                barista_weight()
            elif option == "7":
                break
            break
        else:
            print colored("Sorry, didn't recognize your option, try again", "red")
            time.sleep(2)

def barista_add(name):
    name = name.strip()
    if barista_list.count(name) > 0:
        print colored("They are already in your list", "red")
        time.sleep(2)
        barista_menu()
    elif len(name) == 0 or name.isspace():
        print colored("Nothing entered", "red")
        time.sleep(2)
        barista_menu()
    elif name:
        barista_list.append(name)
        print colored("Barista added: %s", "blue") % name
        print raw_input(colored("Press enter to continue...", "green"))
        barista_menu()

def barista_del(name):
    name = name.strip()
    if barista_list.count(name) < 0:
        print colored("That name does not exist", "red")
        time.sleep(2)
        barista_menu()
    elif len(name) == 0 or name.isspace():
        print colored("Nothing entered", "red")
        time.sleep(2)
        barista_menu()
    elif name:
        if any(item == name for item in barista_list):
            barista_list.remove(name)
            print colored("Barista removed: %s", "blue") % name
            print raw_input(colored("Press enter to continue...", "green"))
            barista_menu()
        else:
            print colored("That name does not exist in our list", "red")
            time.sleep(2)
            barista_menu()

def barista_view():
    if len(barista_list) == 0:
        print colored("List is empty", "red")
        time.sleep(2)
        barista_menu()
    else:
        print "Barista's: " + colored(", ".join(barista_list), "blue")
        print raw_input(colored("Press enter to continue...", "green"))
        barista_menu()

def barista_find(name):
    global weight_name
    global weight
    if barista_list:
        print colored("Your barista is...\n", "green")
        time.sleep(3)
        if len(barista_list) <= 1:
            print colored("You need to add at least 2 names to the list -> ",
            "red") + colored(random.choice(not_kool), "blue")
            time.sleep(5)
            barista_menu()
        elif len(barista_list) > 1:
            barista = random.choice(name)
            while barista == weight_name and weight > 0:
                weight -= 1
                print colored("(%s saved by weight, %s lives left...)", "yellow") % (weight_name, weight)
                barista = random.choice(name)
                time.sleep(1)
            print colored("~~** %s **~~ \n" % barista, "blue")
            weight_name = ""
            weight = 0
            print raw_input(colored("Press enter to continue...", "green"))
            ts = datetime.datetime.now().strftime("%I:%M%p on %B %d, %Y")
            count = barista_stats_check(barista)
            if count < 1:
                barista_stats_update(barista, 1, ts)
                barista_menu()
            else:
                count = barista_stats_check(barista) + 1
                barista_stats_update(barista, count, ts)
                barista_menu()
    else:
        print colored("You have nothing in your list", "red")
        time.sleep(2)
        barista_menu()

def barista_stats():
    barista_stats_fetch()
    print raw_input(colored("Press enter to continue...", "green"))
    barista_menu()

def barista_weight():
    global weight_name
    global weight
    if len(barista_list) == 0:
        print colored("List is empty", "red")
        time.sleep(2)
        barista_menu()
    else:
        print "Barista list: " + colored(", ".join(barista_list), "blue")
        weight_name = raw_input(colored("Enter the name of the barista you wish to weight: ", "green"))
    if barista_list.count(weight_name) < 0:
        print colored("That name does not appear to be in the list", "red")
        time.sleep(2)
        barista_weight()
    elif len(weight_name) == 0 or weight_name.isspace():
        print colored("Nothing entered", "red")
        time.sleep(2)
        barista_weight()
    elif weight_name:
        if any(item == weight_name for item in barista_list):
            weight = int(raw_input("Select a weight from 1-3: "))
            print colored("Barista weight added: %s %s", "blue") % (weight_name, weight)
            print raw_input(colored("Press enter to continue...", "green"))
            barista_menu()
        else:
            print colored("That name does not exist in our list", "red")
            time.sleep(2)
            barista_weight()

barista_menu()
